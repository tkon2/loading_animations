function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  rectMode(CENTER);
}

class Diamond {
  /** Rotation */
  r = 0;
  /** DesiredRotation */
  dr = 0;

  size = 50;

  constructor(size) {
    this.size = size;
  }

  show() {
    const newOff = (this.dr - this.r) * Math.min(1, deltaTime/100);
    fill(255 - this.size + newOff);
    push();
    rotate(this.r);
    square(0,0,this.size);
    pop();
    this.r += newOff;
  }

  turn() {
    this.dr = 90;
  }

  isTurnOver() {
    if (this.r.toFixed(2) !== this.dr.toFixed(2)) return false;
    this.dr = 0;
    this.r = 0;

    return true;
  }
}

const diamonds = [
  new Diamond(150),
  new Diamond(100),
  new Diamond(50),
]

diamonds[2].turn();
let current = 2;

function draw() {
  background(35, 45, 50);
  noStroke();
  translate(width/2, height/2);
  rotate(45);
  for (let i = 0; i < diamonds.length; i++) {
    diamonds[i].show();

    if (current !== i) continue;
    if (!diamonds[i].isTurnOver()) continue;

    current = (diamonds.length + current - 1) % diamonds.length;
    diamonds[current].turn();
  }
}

