function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  rectMode(CENTER);
}

const MOVEMENT = 10;

class Square {
  /** Position */
  x = -MOVEMENT;
  y = -MOVEMENT;
  /** DesiredPosition */
  dx = -MOVEMENT;
  dy = -MOVEMENT;

  size = 72;

  show() {
    const newXOff = (this.dx - this.x) * Math.min(1, deltaTime/100);
    const newYOff = (this.dy - this.y) * Math.min(1, deltaTime/100);
    fill(128 + Math.max(newXOff, newYOff));
    square(this.x, this.y,this.size);
    this.x += newXOff;
    this.y += newYOff;
  }

  turn() {
    if (this.x < 0 && this.y < 0) this.dx = MOVEMENT;
    if (this.x > 0 && this.y < 0) this.dy = MOVEMENT;
    if (this.x > 0 && this.y > 0) this.dx = -MOVEMENT;
    if (this.x < 0 && this.y > 0) this.dy = -MOVEMENT;
  }

  isTurnOver() {
    if (this.x.toFixed(2) !== this.dx.toFixed(2)) return false;
    if (this.y.toFixed(2) !== this.dy.toFixed(2)) return false;
    this.x = this.dx;
    this.y = this.dy;
    return true;
  }
}

const diamonds = [
  new Square(),
]

diamonds[0].turn();
let current = 0;

function draw() {
  background(35, 45, 50);
  noStroke();
  translate(width/2, height/2);
  for (let i = 0; i < diamonds.length; i++) {
    diamonds[i].show();

    if (current !== i) continue;
    if (!diamonds[i].isTurnOver()) continue;

    current = (diamonds.length + current - 1) % diamonds.length;
    diamonds[current].turn();
  }

  fill(255);
  square(0,0,50);
}

