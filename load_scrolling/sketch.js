const loadingText = "loading";
const textHeight = 40;

let bgColor;
let hlColor;

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  textAlign(CENTER, CENTER);
  bgColor = color(35,45,50, 50);
  hlColor = color(255, 128, 0);
  hlColor.setAlpha(255);
}

let xOff = 0;
let brightOff = 0;

function draw() {
  const tw = textWidth(loadingText) + 20;
  const th = textHeight + 16;
  xOff = (xOff + deltaTime / 25) % tw;
  // const amx = mouseX - width/2;
  // const amy = mouseY - height/2;
  // const mx = amx * cos(-45) + amy * sin(-45);
  // const my = -amx * sin(-45) + amy * cos(-45);
  const mx = 0;
  const my = 0;
  background(35, 45, 50);
  noStroke();
  translate(width/2, height/2);
  rotate(-45);
  textSize(textHeight);
  let evenness = false;
  for (let j = -20 * th; j < 20 * th; j += th) {
    evenness = !evenness;
    for (let i = -20 * tw; i < 20 * tw; i+= tw) {
      if (Math.abs(i) + Math.abs(j) > 1600) continue;
      const x = i + (xOff * (evenness ? 1 : -1));
      const y = j;
      const dist = Math.sqrt(Math.pow(mx-x, 2) + Math.pow(my-y, 2));
      fill(lerpColor(bgColor, hlColor, 50/dist), 45, 50);
      text(loadingText, x, y);
    }
  }
}
